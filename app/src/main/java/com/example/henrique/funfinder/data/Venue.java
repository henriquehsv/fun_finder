package com.example.henrique.funfinder.data;

import android.net.Uri;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

/**
 * Represents a location from foursquare.
 */
public class Venue {
    private String id;
    private String name;
    private String contact;

    private VenueLocation location;

    private String category;

    private Uri url;

    private BigDecimal rating;
    private String ratingColor;

    private String photoUri;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getContact() {
        return contact;
    }

    public String getCategory() {
        return category;
    }

    public Uri getUrl() {
        return url;
    }

    public BigDecimal getRating() {
        return rating;
    }

    public String getRatingColor() {
        return ratingColor;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public VenueLocation getLocation() {
        return location;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
