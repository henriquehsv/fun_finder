package com.example.henrique.funfinder.data.source;

import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FourSquareExplore {
    @GET("venues/explore?venuePhotos=1&")
    Call<JsonElement> getVenueRecommendations(@Query("ll") String latLong, @Query("query") String query);
}