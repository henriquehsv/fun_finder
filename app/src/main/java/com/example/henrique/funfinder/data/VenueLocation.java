package com.example.henrique.funfinder.data;

public class VenueLocation {
    private String address;
    private String crossStreet;
    private String neighborhood;
    private String city;

    private double lat;
    private double lng;

    public String getAddress() {
        return address;
    }

    public String getCrossStreet() {
        return crossStreet;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public String getCity() {
        return city;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}
