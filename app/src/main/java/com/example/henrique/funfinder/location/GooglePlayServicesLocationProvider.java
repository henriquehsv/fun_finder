package com.example.henrique.funfinder.location;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

public class GooglePlayServicesLocationProvider implements LocationProvider {
    private final FusedLocationProviderClient locationProviderClient;

    public GooglePlayServicesLocationProvider(Context context) {
        locationProviderClient = LocationServices.getFusedLocationProviderClient(context);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void getLastKnownLocation(LocationRetrievedListener listener) {
        locationProviderClient
                .getLastLocation()
                .addOnSuccessListener(location -> doOnLocationRetrieved(listener, location))
                .addOnFailureListener(e -> listener.onFailure());
    }

    private void doOnLocationRetrieved(LocationRetrievedListener listener, Location location) {
        if (location != null) {
            listener.onSuccess(location.getLatitude(), location.getLongitude());
        } else {
            listener.onFailure();
        }
    }
}
