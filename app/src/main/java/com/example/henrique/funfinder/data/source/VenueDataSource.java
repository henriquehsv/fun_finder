package com.example.henrique.funfinder.data.source;

import com.example.henrique.funfinder.data.Venue;

import java.util.List;

/**
 * Data source for Venue.
 * This class is used as a external communication point between the app and the web server.
 * <p>
 * We have only one source of data, which is the web.
 * This class could be extended to support fetching data from a local cache as well.
 */
public interface VenueDataSource {
    interface VenuesCallback {
        void onVenuesFetched(List<Venue> venues);

        void onError();
    }

    void fetchVenues(double lat, double lng, String query, VenuesCallback venuesCallback);
}
