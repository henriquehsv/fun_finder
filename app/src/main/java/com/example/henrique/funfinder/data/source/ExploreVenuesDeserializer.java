package com.example.henrique.funfinder.data.source;

import com.example.henrique.funfinder.data.Venue;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Custom deserializer to extract a list of venues from a foursquare Explore endpoint.
 * Details of the endpoint can be found at:
 * <p>
 * https://developer.foursquare.com/docs/api/venues/explore
 * <p>
 * We need to go deep into the Json hierarchy until we're able to find a venue:
 * An example would be:
 * <p>
 * "groups": [
 * {
 * "items": [
 * "0": {
 * "venue": ...
 * },
 * ]
 * }
 * ]
 */
public class ExploreVenuesDeserializer implements JsonDeserializer<List<Venue>> {
    private final Gson gson;

    public ExploreVenuesDeserializer() {
        gson = new Gson();
    }

    @Override
    public List<Venue> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject rootElement = json.getAsJsonObject();

        // Each list of items will contain one venue element
        JsonArray groups = rootElement.get("response").getAsJsonObject().get("groups").getAsJsonArray();
        if (groups.size() == 0) {
            return null;
        }
        JsonArray items = groups.get(0).getAsJsonObject().get("items").getAsJsonArray();

        List<Venue> venues = new ArrayList<>(items.size());

        for (JsonElement item : items) {
            JsonObject venueJsonObject = item.getAsJsonObject()
                    .get("venue")
                    .getAsJsonObject();

            venues.add(decodeVenueObject(venueJsonObject));
        }

        return venues;
    }

    private Venue decodeVenueObject(JsonObject venueJson) {
        Venue venue = gson.fromJson(venueJson, Venue.class);

        for (JsonElement category : venueJson.getAsJsonArray("categories")) {
            JsonObject categoryJson = category.getAsJsonObject();

            if (categoryJson.has("primary")) {
                venue.setCategory(categoryJson.get("name").getAsString());
                break;
            }
        }

        return venue;
    }
}
