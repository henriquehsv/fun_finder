package com.example.henrique.funfinder.search;


import com.example.henrique.funfinder.data.Venue;
import com.example.henrique.funfinder.data.source.VenueDataSource;
import com.example.henrique.funfinder.location.LocationProvider;

import java.util.List;

public class SearchPresenter implements SearchContract.Presenter {

    private SearchContract.View view;
    private VenueDataSource dataSource;
    private LocationProvider locationProvider;

    public SearchPresenter(SearchContract.View view, VenueDataSource dataSource, LocationProvider locationProvider) {
        this.view = view;
        this.dataSource = dataSource;
        this.locationProvider = locationProvider;
    }

    @Override
    public void onSearchButtonPressed() {
        view.showLoadingIndicator(true);
        if (view.checkLocationPermission()) {
            fetchVenues();
        } else {
            view.requestLocationPermission();
        }
    }

    @Override
    public void onLocationPermissionResult(boolean granted) {
        if (granted) {
            fetchVenues();
        } else {
            onErrorFetchingLocation();
        }
    }

    private void onVenuesRetrieved(List<Venue> venues) {
        view.showLoadingIndicator(false);
        view.displaySearchResults(venues);
    }

    private void onErrorFetchingLocation() {
        view.showLoadingIndicator(false);
        view.displayResultLoadingErrorMessage();
    }

    private void onErrorFetchingVenues() {
        view.showLoadingIndicator(false);
        view.displayResultLoadingErrorMessage();
    }

    private void fetchVenues() {
        locationProvider.getLastKnownLocation(new LocationProvider.LocationRetrievedListener() {
            @Override
            public void onSuccess(double lat, double lng) {
                dataSource.fetchVenues(lat, lng, view.getQuery(), new VenueDataSource.VenuesCallback() {
                    @Override
                    public void onVenuesFetched(List<Venue> venues) {
                        onVenuesRetrieved(venues);
                    }

                    @Override
                    public void onError() {
                        onErrorFetchingVenues();
                    }
                });
            }

            @Override
            public void onFailure() {
                onErrorFetchingVenues();
            }
        });
    }

    @Override
    public void start() {
        view.showLoadingIndicator(false);
    }
}
