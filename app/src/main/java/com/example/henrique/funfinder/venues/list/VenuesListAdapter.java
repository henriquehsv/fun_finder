package com.example.henrique.funfinder.venues.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.henrique.funfinder.R;
import com.example.henrique.funfinder.data.Venue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VenuesListAdapter extends RecyclerView.Adapter<VenuesListAdapter.VenuesViewHolder> {

    private List<Venue> venues;

    public VenuesListAdapter() {
        venues = new ArrayList<>();
    }

    public void setVenues(List<Venue> venues) {
        this.venues = Collections.unmodifiableList(venues);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return venues.get(position).getId().hashCode();
    }

    @Override
    public VenuesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View venueView = inflater.inflate(R.layout.venue_item, parent, false);

        return new VenuesViewHolder(venueView);
    }

    @Override
    public void onBindViewHolder(VenuesViewHolder holder, int position) {
        Venue venue = venues.get(position);

        holder.name.setText(String.format(holder.name.getContext().getString(R.string.venue_name), position, venue.getName()));
        holder.type.setText(venue.getCategory());
        holder.address.setText(venue.getLocation().getAddress());
    }

    @Override
    public int getItemCount() {
        return venues.size();
    }

    public static class VenuesViewHolder extends RecyclerView.ViewHolder {
        private final TextView type;
        private final TextView name;
        private final TextView address;

        public VenuesViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.venue_name);
            address = itemView.findViewById(R.id.venue_address);
            type = itemView.findViewById(R.id.venue_type);
        }
    }

}
