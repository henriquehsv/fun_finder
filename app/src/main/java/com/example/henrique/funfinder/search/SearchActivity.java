package com.example.henrique.funfinder.search;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.example.henrique.funfinder.R;
import com.example.henrique.funfinder.data.Venue;
import com.example.henrique.funfinder.data.source.VenueRepository;
import com.example.henrique.funfinder.location.GooglePlayServicesLocationProvider;
import com.example.henrique.funfinder.venues.list.VenuesListAdapter;

import java.util.List;

public class SearchActivity extends AppCompatActivity implements SearchContract.View {
    private static final int LOCATION_PERMISSION_REQUEST = 1;

    private SearchContract.Presenter presenter;

    private VenuesListAdapter adapter;

    private SearchView searchBox;

    private SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        getSupportActionBar().setElevation(0);

        presenter = new SearchPresenter(this, new VenueRepository(), new GooglePlayServicesLocationProvider(this));
        searchBox = findViewById(R.id.search_box);

        refreshLayout = findViewById(R.id.refresh_layout);
        refreshLayout.setEnabled(false);

        searchBox.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                presenter.onSearchButtonPressed();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        RecyclerView venuesList = findViewById(R.id.venues_list);
        venuesList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        adapter = new VenuesListAdapter();
        venuesList.setAdapter(adapter);

        DividerItemDecoration spaceSeparation = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        spaceSeparation.setDrawable(getDrawable(R.drawable.space_separator));

        venuesList.addItemDecoration(spaceSeparation);

        presenter.start();
    }

    @Override
    public void showLoadingIndicator(boolean enabled) {
        refreshLayout.setRefreshing(enabled);
    }

    @Override
    public void displaySearchResults(@NonNull List<Venue> results) {
        adapter.setVenues(results);
        hideKeyboard();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }

    @Override
    public boolean checkLocationPermission() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)) {
            showPermissionExplanationDialog();
        } else {
            // No explanation needed; request the permission
            showPermissionRequestDialog();
        }
    }

    @Override
    public void displayResultLoadingErrorMessage() {
        Toast.makeText(this, R.string.venue_loading_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayLocationPermissionErrorMessage() {
        Toast.makeText(this, R.string.location_permission_granted_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public String getQuery() {
        return searchBox.getQuery().toString();
    }

    private void showPermissionExplanationDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.permission_explanation_message)
                .setOnDismissListener(v -> this.showPermissionRequestDialog())
                .create()
                .show();
    }

    private void showPermissionRequestDialog() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                LOCATION_PERMISSION_REQUEST);
    }

    public void setPresenter(SearchContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST: {
                presenter.onLocationPermissionResult(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED);
            }
        }
    }
}
