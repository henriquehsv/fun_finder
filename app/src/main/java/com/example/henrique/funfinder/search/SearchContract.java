package com.example.henrique.funfinder.search;

import android.support.annotation.NonNull;

import com.example.henrique.funfinder.BasePresenter;
import com.example.henrique.funfinder.BaseView;
import com.example.henrique.funfinder.data.Venue;

import java.util.List;

public class SearchContract {
    interface View extends BaseView {
        void showLoadingIndicator(boolean enabled);

        void displaySearchResults(@NonNull List<Venue> results);

        boolean checkLocationPermission();

        void requestLocationPermission();

        void displayResultLoadingErrorMessage();

        void displayLocationPermissionErrorMessage();

        String getQuery();
    }

    interface Presenter extends BasePresenter {
        void onSearchButtonPressed();

        void onLocationPermissionResult(boolean granted);
    }
}
