/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.henrique.funfinder.data.source;

import com.example.henrique.funfinder.MainApplication;
import com.example.henrique.funfinder.data.Venue;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Concrete implementation of a {@link VenueDataSource}.
 */
public class VenueRepository implements VenueDataSource {
    private static final String BASE_EXPLORE_URL = "https://api.foursquare.com/v2/";
    private static final Type VENUE_LIST_TYPE = new TypeToken<List<Venue>>() {}.getType();

    private final Gson gson;

    public VenueRepository() {
        gson = new GsonBuilder()
                .registerTypeAdapter(VENUE_LIST_TYPE, new ExploreVenuesDeserializer())
                .create();
    }

    @Override
    public void fetchVenues(double lat, double lng, String query, VenuesCallback venuesCallback) {
        if (query.isEmpty()) {
            venuesCallback.onError();
            return;
        }

        Retrofit retrofit = MainApplication.getInstance()
                .getRetrofitClient(BASE_EXPLORE_URL);

        FourSquareExplore explore = retrofit.create(FourSquareExplore.class);

        explore.getVenueRecommendations(String.format(Locale.US, "%.5f,%.5f", lat, lng), query)
                .enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        if (response.isSuccessful()) {
                            venuesCallback.onVenuesFetched(gson.fromJson(response.body(), new TypeToken<List<Venue>>(){}.getType()));
                        } else {
                            venuesCallback.onError();
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        venuesCallback.onError();
                    }
                });
    }

}
