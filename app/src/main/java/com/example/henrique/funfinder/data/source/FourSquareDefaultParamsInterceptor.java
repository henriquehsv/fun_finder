package com.example.henrique.funfinder.data.source;

import android.support.annotation.NonNull;

import com.example.henrique.funfinder.BuildConfig;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Adds required authentication params to Foursquare urls
 */
public class FourSquareDefaultParamsInterceptor implements Interceptor {

    private static final String LAST_SUPPORTED_VERSION = "20180805";

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request originalRequest = chain.request();

        HttpUrl originalUrl = originalRequest.url();
        HttpUrl url = originalUrl.newBuilder()
                .addQueryParameter("client_id", BuildConfig.FOURSQUARE_CLIENT_ID)
                .addQueryParameter("client_secret", BuildConfig.FOURSQUARE_CLIENT_SECRET)
                .addQueryParameter("v", LAST_SUPPORTED_VERSION)
                .build();

        Request.Builder requestBuilder = originalRequest.newBuilder().url(url);
        Request request = requestBuilder.build();

        return chain.proceed(request);
    }
}
