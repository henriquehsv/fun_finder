package com.example.henrique.funfinder.location;

public interface LocationProvider {
    void getLastKnownLocation(LocationRetrievedListener listener);

    interface LocationRetrievedListener {
        void onSuccess(double lat, double lng);
        void onFailure();
    }
}
