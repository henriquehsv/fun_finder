package com.example.henrique.funfinder;

import android.app.Application;
import android.support.annotation.NonNull;

import com.example.henrique.funfinder.data.source.FourSquareDefaultParamsInterceptor;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainApplication extends Application {
    private static final int CACHE_SIZE_10_MB = 10 * 1024 * 1024;
    private OkHttpClient okHttp;

    private static MainApplication instance;


    public static MainApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        okHttp = new OkHttpClient
                .Builder()
                .cache(new Cache(getCacheDir(), CACHE_SIZE_10_MB))
                .addInterceptor(new FourSquareDefaultParamsInterceptor())
                .build();
    }

    public synchronized Retrofit getRetrofitClient(@NonNull String baseURL) {
        return new Retrofit.Builder()
                .client(okHttp)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseURL)
                .build();
    }

}
