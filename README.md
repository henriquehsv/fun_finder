# FunFinder
### A simple app to query 4square and search for venues near users
---

If you want to run this app on your computer, you'll need a [4square client token and secret](https://developer.foursquare.com/):

After getting your credentials, you'll need to add them to `local.properties` in the repo's root folder. e. g.

```
foursquare.client.id="<your-client-id>"
foursquare.client.secret="<your-client-secret>"
```

I've not added them to version control, as this would be a security issue.


